package es.upm.babel.cclib;

import java.util.concurrent.locks.*;
import java.util.HashMap;

/**
 * This library is used to log messages for a specific system or
 * application component. Methods are thread-safe.
 */
public class Logger {
  static private Lock wrLock = new ReentrantLock();
  static private long initialTimeMillis = System.currentTimeMillis();
  static private long nextId = 0;
  // Maps PIDs to an internal correlative thread id in this logger
  static private HashMap<Long,Long> threadIds = new HashMap<Long,Long>();

  private Logger() {
  }

  // This method is not thread safe!!!
  static private long registerThread() {
    long pid = Thread.currentThread().getId();
    long id = threadIds.getOrDefault(pid, nextId);
    if (id == nextId) {
      threadIds.put(pid, nextId);
      nextId++;
    }
    return id;
  }

  /**
   * Log a format message, with an array of object arguments.
   *
   * @param format - The format message
   * @param args - array of parameters to the format message
   */
  static public void log(String format, Object... args) {
       wrLock.lock();
       long millis = System.currentTimeMillis() - initialTimeMillis;
       String message = String.format(format, args);
       long id = registerThread();
       String threadPrefix = "";
       for (int i = 0; i < id; i++)
         threadPrefix = threadPrefix.concat("\t");
       threadPrefix = String.format("%s[Thread %d] -> ",
                                    threadPrefix,
                                    id);
       System.out.printf("%6d: %s%s\n",
                         millis,
                         threadPrefix,
                         message);
       wrLock.unlock();
  }
}

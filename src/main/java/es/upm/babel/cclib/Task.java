package es.upm.babel.cclib;

import java.util.function.Supplier;

/**
 * A {@code Task<T>} is a process that executes a given code
 * asynchronously. Tasks can be inspected with methods isFinished,
 * raisedException and getResult (with result of type T).
 */
public class Task<T> {
  volatile private boolean started = false;
  volatile private boolean finished = false;
  volatile private Throwable raisedException = null;
  volatile T result = null;

  private Task() {
  }

  public static Task<Void> async(Runnable operation) {
    Task<Void> task = new Task<Void>();
    Runnable tryer = () -> {
      try {
        task.started = true;
        operation.run();
        task.finished = true;
      }
      catch (Throwable t) {
        task.raisedException = t;
      }
    };
    new Thread(tryer).start();
    task.awaitStarted();
    return task;
  }

  public static <T> Task<T> async(Supplier<T> operation) {
    Task<T> task = new Task<T>();
    Runnable tryer = () -> {
      try {
        task.started = true;
        task.result = operation.get();
        task.finished = true;
      }
      catch (Throwable t) {
        task.raisedException = t;
      }
    };
    new Thread(tryer).start();
    task.awaitStarted();
    return task;
  }

  public boolean isFinished() {
    awaitStarted();
    return finished;
  }

  public Throwable raisedException() {
    awaitStarted();
    return raisedException;
  }

  public T getResult() {
    return result;
  }

  private void awaitStarted() {
    while (!started);
  }
}
